<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddApiTokenToUsersTable extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
           // Dodavanje na kolona ' api_token ' vo users tabelata
            $table->string('api_token', 80);
        });
        // So foreach dodavanje na vrednosti na postoeckite useri vo users tabelata
        $users = User::all();
        foreach($users as $user) {
            $user->api_token = Str::random(80);
            $user->save();
        }
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
