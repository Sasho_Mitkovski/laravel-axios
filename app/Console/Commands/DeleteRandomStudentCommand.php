<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class DeleteRandomStudentCommand extends Command
{

    protected $signature = 'brainster:delete-random-student';

    protected $description = 'Random brisenje na student';

    public function handle()
    {
        $role = Role::where('name', 'student')->first();
        $user = User::where('role_id', $role->id)->inRandomOrder()->first();
        $name = $user->name;
        $user->delete();

        $this->info("Korisnikot {$name} e izbrishan");
    }
}
