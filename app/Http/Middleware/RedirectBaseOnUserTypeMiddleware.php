<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RedirectBaseOnUserTypeMiddleware
{

    public function handle(Request $request, Closure $next,$role_name)
    {
       $user_role = auth()->user()->role->name;
        if($user_role == $role_name){
          return $next($request);
        }

        if ($user_role == 'student') {
            return redirect()->route('home');
        }

        if( $user_role == 'instructor' ) {
            return redirect()->route('admin');
        }
    }
}
