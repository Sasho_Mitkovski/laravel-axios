<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UsersApiController extends Controller
{
    // ========================  INDEX  ===============================================================
    public function index()
    {
        //Koga rabotime so foreach, preporaclivo e so with da go pravime query-to
        $users = User::with('role')->get(); //Za da ni dava i model od Roles tabelata

        // So ovie informacii ke rabotime na frontend
        $json = [];
        foreach ($users as $user) {
            $json[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role->name,
            ];
        }
        // $users->map->only(['id', 'name', 'email', 'role_name']);// Moze vaka, a i kako pogore vo kodot
        return response()->json($json);
    }

    public function store(Request $request)
    {
        //
    }
    // ========================  SHOW  ===============================================================
    public function show($id)
    {
        $user = User::find($id);

        // return response()->json($user);
        return response()->json([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role->name,
        ]);
    }
    // ========================  UPDATE  ===============================================================
    public function update(Request $request, $id)
    {
        // Validacija
        $request->validate([
            'name' => 'required',
            'email' => ['required', Rule::unique('users')->ignore($id)]
        ]);
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        $user->save();

        return response()->json([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role->name,
        ]);
    }
    // ========================  DESTROY (delete) ===============================================================
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json([
            'message' => 'User deleted'
        ]);
    }
    // ================================== SEARCH ========================================================
    public function search($q)
    {
        $users = User::where('email', 'LIKE', "%{$q}%")->orWhere('name', 'LIKE', "%{$q}%")->get();

        $json = [];
        foreach ($users as $user) {
            $json[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => $user->role->name,
            ];
        }
        return response()->json($json);
    }
}
