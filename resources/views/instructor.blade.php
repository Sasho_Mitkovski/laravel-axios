@extends('layouts.app')
@section('content')
    <div id="users-list" class="container">
        <form>
            <input type="text" name="q" id="q" class="form-control" placeholder="Search user - name ili email"/>
        </form>
        <table id="users" class="table">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
            </thead>
            <tbody>
                <td colspan="5">Loading users ....</td>
            </tbody>
        </table>
    </div>
    {{-- ==================  MODAL ================================================================================ --}}
    <div class="modal fade" id="EditUser" tabindex="-1" aria-labelledby="EditUserLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="EditUserLabel">Edit User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        {{-- Name --}}
                        <input type="hidden" name="id" id="user-id" value="" />
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name:</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <div class="errors text-warning"></div>
                        </div>
                        {{-- Email --}}
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email:</label>
                            <input type="text" class="form-control" id="email" name="email">
                            <div class="errors text-warning"></div>
                        </div>
                    </form>
                    {{-- Printanje na errors --}}
                    <div id="user-errors"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="update-user">Update user</button>
                </div>
            </div>
        </div>
    </div>
    {{-- ======================================  MODAL za delete  ========================================================================= --}}

  <!-- Modal -->
  <div class="modal fade" id="DeleteUser" tabindex="-1" aria-labelledby="DeleteUserLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="DeleteUserLabel">Delete user?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure
          <input type="hidden" name="delete-user-id" id="delete-user-id"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger" id="delete-user-button">Delete</button>
        </div>
      </div>
    </div>
  </div>

@endsection
