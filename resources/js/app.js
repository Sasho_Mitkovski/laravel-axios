require("./bootstrap");

// ============================================ Funkcija za printanje row vo tabela ===================================================
function add_user_to_table(user) {
    var actions = "N/A";
    //   Ako role.name e MediaStreamAudioDestinationNode, pokazi actions button
    if (user.role == "student") {
        actions = `
    <button class="btn btn-info" data-toggle="modal" data-target="#EditUser" data-user-id="${user.id}">Edit</button>
    <button class="btn btn-danger" data-toggle="modal" data-target="#DeleteUser" data-user-id="${user.id}">Delete</button>
    `;
    }

    $("#users tbody").prepend(`
    <tr data-user-id="${user.id}">
   <td>${user.id}</td>
   <td>${user.name}</td>
   <td>${user.email}</td>
   <td>${user.role}</td>
   <td>${actions}</td>
   </tr>
   `);
}
//======================== Printanje vo tabela =========================================
var table = $("#users-list");
if (table.length) {
    // Uslov ako na stranata e pronajden ID user-list, ke se izvrsuva kodot
    axios
        .get("/api/users")
        .then(function (response) {
            var users = response.data;
            console.log(users);
            //Printanje na users vo tabelata
            $("#users tbody").html("");
            $(users).each(function (i, user) {
                add_user_to_table(user);
                // var actions = "N/A";
                // //   Ako role.name e MediaStreamAudioDestinationNode, pokazi actions button
                // if (user.role == "student") {
                //     actions = `
                // <button class="btn btn-info" data-toggle="modal" data-target="#EditUser" data-user-id="${user.id}">Edit</button>
                // <button class="btn btn-danger" data-toggle="modal" data-target="#DeleteUser" data-user-id="${user.id}">Delete</button>
                // `;
                // }

                // $("#users tbody").prepend(`
                //  <tr data-user-id="${user.id}">
                // <td>${user.id}</td>
                // <td>${user.name}</td>
                // <td>${user.email}</td>
                // <td>${user.role}</td>
                // <td>${actions}</td>
                // </tr>
                // `);
            });
        })
        .catch(function (error) {
            console.log(error);
        });

    //================================= SHOW user  ===========================================================================
    $("#EditUser").on("show.bs.modal", function (event) {
        var clickedButton = $(event.relatedTarget);

        var id = clickedButton.attr("data-user-id");
        axios
            .get(`/api/users/${id}`)
            .then((res) => {
                var user = res.data;
                //    $('#EditUserLabel').text("Edit " + (user.name) + " details:")
                $("#EditUserLabel").text(`Update ${user.name} details:`);
                //Zemanje na vrednosti vo input polinja
                $("#EditUser #user-id").val(user.id);
                $("#EditUser #name").val(user.name);
                $("#EditUser #email").val(user.email);
            })
            .catch((err) => {
                console.log("err", err);
            });
    });
    // ============================ UPDATE user ========================================================
    // Mestime na klik na button Update user vo modalot
    $("#update-user").on("click", function (event) {
        event.preventDefault();

        var id = $("#EditUser #user-id").val();
        // data e toa sto treba da go pratime vo baza preku method update
        var data = {
            name: $("#EditUser #name").val(),
            email: $("#EditUser #email").val(),
        };

        axios
            .post(`/api/users/${id}/update`, data)
            .then((res) => {
                // Printanje na row vo tabelata na noviot apdajtiran user
                var user = res.data;
                //funkcija za printanje na row
                add_user_to_table(user);
                $("#EditUser").modal("hide");
            })

            // Printanje na errors
            .catch((err) => {
                var errors = err.response.data.errors;
                $("#user-errors").html("");
                // Object.values(errors).forEach(function (fieldName) {
                //     $("#user-errors").append(
                //         "<p>" + fieldName.join(". ") + "</p>"
                //     )
                // })
                Object.keys(errors).forEach(function (fieldName) {
                    //('#' + fieldName)-ova e id,odi nagore i najdi form-group, i vo nego najdi errors
                    $("#" + fieldName)
                        .closest(".form-group")
                        .find(".errors.text-warning")
                        .html("");
                    $("#" + fieldName)
                        .closest(".form-group")
                        .find(".errors.text-warning")
                        .html(errors[fieldName].join(". "));
                });
            });
        // $('#EditUser').modal('hide')// krienje na modalot
    });

    //=================================================== Destroy ==========================================================

    $("#DeleteUser").on("show.bs.modal", function (event) {
        var clickedButton = $(event.relatedTarget);
        var id = clickedButton.attr("data-user-id");
        $("#delete-user-id").val(id); //ID e dugmeto sto e kliknato
    });

    $("#delete-user-button").on("click", function (event) {
        event.preventDefault();
        var id = $("#delete-user-id").val();
        axios
            .post(`/api/users/${id}/delete`)
            .then((res) => {
                $("#DeleteUser").modal("hide");
                $(`#users tbody tr[data-user-id="${id}"]`).remove(); //brisenje na ROW OD TABELATA
            })
            // Printanje na errors
            .catch((err) => {
                console.log("err", err);
            });
    });

    //============================================ SEARCH method ===============================================================================
    $("#q").on("keyup", function (event) {
        var q = $("#q").val();
        if (q && q.length > 0) {
            axios
                .get("/api/users/search/" + q)
                .then((response) => {
                    var users = response.data;
                    // Funkcija za printanje vo tabela, kreirana na pocetok na page
                    $("#users tbody").html("");
                    // Ako ima users, printaj gi vo tabela
                    if (users.length > 0) {
                        $(users).each(function (i, user) {
                            add_user_to_table(user);
                        })
                    } else {
                        $('#users tbody').html(`
                        <tr colspan="5">
                            <td>No results found for <strong>${q}</strong></td>
                        </tr>
                    `)
                    }
                })
                .catch(err => {
                    console.log('err', err)
                });
        }
    });
}
