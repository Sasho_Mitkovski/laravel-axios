<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
||Тука е местото каде што можете да регистрирате веб-патеки за вашата апликација.Овие
|правци се натоварени од страна на русетницата во групата која
|Содржи "Веб" Middleware Group.Сега создадете нешто одлично!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('redirect-base-user-type:student');
Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin')->middleware('redirect-base-user-type:instructor');
