<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:api', 'redirect-base-user-type:instructor'])->group(function(){
Route::get('users', [App\Http\Controllers\UsersApiController::class, 'index']);
Route::get('users/{id}', [App\Http\Controllers\UsersApiController::class, 'show']);
Route::post('users/{id}/update', [App\Http\Controllers\UsersApiController::class, 'update']);
Route::post('users/{id}/delete', [App\Http\Controllers\UsersApiController::class, 'destroy']);
Route::get('users/search/{q}', [App\Http\Controllers\UsersApiController::class, 'search']);
});
